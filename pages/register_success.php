<!DOCTYPE html>
<!--
Copyright (C) 2013 peredur.net

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Login: Registration Success</title>
        <link rel="stylesheet" href="../css/main.css" />
    </head>
    <body background="border.jpg">
        <div class="form-success">
            <div class="field">
        <font class="text_success">Registration successful!</font>
                </div>
        <font class="text_login">You can now go back to the <a class="a_register" style="text-decoration: underline;" href="../pages/index.php">login page</a> and log in</font>
        </div>
    </body>
</html>
