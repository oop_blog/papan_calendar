<?php
    include_once '../includes/functions.php';
    sec_session_start();
    if(isset($_SESSION['username']))
    {
        $session_username = htmlentities($_SESSION['username']);
    } 
    else $session_username = "Guest";
    date_default_timezone_set("Asia/Bangkok"); //set time zone
    $now = 'now'; // declare now for use in find year-month-day
    if(isset($_GET['now']) && !empty($_GET['now'])){
    $now = $_GET['now'];
    }
    
    $day = date('d',strtotime($now)); 
    $week = date('w',strtotime($now));

    //echo $day.'<br>';
    //echo $week.'<br>';

    $month = date('m',strtotime($now)); // get month for use in fucntion next and prev.
    $year = date('Y', strtotime($now)); // get year for use in fucntion next and prev.
    $firstday = date('w', strtotime('01-'.$month.'-'.$year)); // get day of week
    $days = date('t',strtotime($now)); // get number of days in month
    $today = date('d'); // get today
    $todaymonth = date('m'); // get month
    $todayyear = date('Y'); // get year

    $start = date("Y-m-d",strtotime("".($now)."-".($week)." days"));
    $end = date("Y-m-d",strtotime("".($now)."+".(6-$week)." days"));
    //echo "ประกอบด้วยวันที่ : ".$start." - ".$end;

    //echo $start.'<br>';
    //echo $end;


    $start_day = date('d',strtotime($start));
    $end_day = date('d',strtotime($end));


    $arr_date_day = array();
    $arr_date_month = array();
    for($z=0;$z<7;$z++){
    $arr_date_day[$z] = $start_day = date('d',strtotime("+$z day $start"));
         $arr_date_month[$z] = $start_day = date('m',strtotime("+$z day $start"));
        
    }

    $next_month = strtotime("+1 week $year-$month-$day"); // calculate timestamp next month
    $last_month = strtotime("-1 week $year-$month-$day"); // calculate timestamp last month 
     
    
    $get_next_year = date('Y', $next_month); // get year from $last_month
    $get_last_year = date('Y', $last_month); // get year from $next_month

    $get_next_month = date('m', $next_month); // get month from $last_month
    $get_last_month = date('m', $last_month); // get month from $next_month

    $get_next_day = date('d', $next_month); // get month from $last_month
    $get_last_day = date('d', $last_month); // get month from $next_month
    
    $to_lastmonth = "<a href=\"" . '?now=' . $get_last_year .'-'. $get_last_month . '-'. $get_last_day ."\"> ❮ </a>"; // link to lastmonth
    $to_nextmonth = "<a href=\"" . '?now=' . $get_next_year .'-'. $get_next_month . '-'. $get_next_day . "\"> ❯ </a>"; // link to nextmonth

    $check_month = $month; // check month to string
    switch ($check_month) {
        case "01":
        $check_month = "January";
        break;
        case "02":
         $check_month = "February";
        break;
        case "03":
         $check_month = "March";
        break;
        case "04":
         $check_month = "April";
        break;
        case "05":
         $check_month = "May";
        break;
        case "06":
         $check_month = "June";
        break;
        case "07":
         $check_month = "July";
        break;
        case "08":
         $check_month = "August";
        break;
        case "09":
         $check_month = "September";
        break;
        case "10":
         $check_month = "October";
        break;
        case "11":
         $check_month = "November";
        break;
        case "12":
         $check_month = "December";
        break;
}  
?>


<html>
<head>
    <title>Calendar</title>
    <link rel="stylesheet" href="../css/week.css" type="text/css">
    <link rel="stylesheet" href="../css/main.css" type="text/css">
    <link rel="stylesheet" href="../jquery-ui-1.12.1/jquery-ui.css">
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../jquery-ui-1.12.1/jquery-ui.js"></script>
</head>

<body>
    <header>
        <div class="nav">
             <ul>
                 <li style="float:left"><a href="../pages/appointment.php">Appointment</a></li>
                <li style="float:left"><a href="week.php?now=<?=$get_last_year;?>-<?=$get_last_month;?>-<?=$get_last_day;?>">Last week</a></li>
                <li class="dropdown" style="float:letf">
                    <a href="#" class="dropbtn">≡ Week</a>
                    <div class="dropdown-content">
                    <a href="month.php">Month</a>
                    <a href="day.php">Day</a>
                    </div>
                    
                </li>
                <li style="float:left"><a href="week.php?now=<?=$get_next_year;?>-<?=$get_next_month;?>-<?=$get_next_day;?>">Next week</a></li>
                <<?php if($session_username == "Guest") { ?>   
            <li style="float:right"><a href="../pages/index.php">Log In</a></li>
            <li style="float:right"><a href="../pages/register.php">Sign Up</a></li>
            <?php } else { ?>
             <li style="float:right"><a href="../includes/logout.php">Logout</a></li>
            <li style="float:right"><a href="#"><?php echo $session_username; ?></a></li>
            <?php } ?>
            </ul>
        </div>
    </header>
    <div class="head-date">
        <font class="text_login">
    <?php echo  $check_month."/".$year?>
        </font>
    </div>
    
    <div class="crop_calendar">
    <div class="calendar">
        <?php  
        $arr_day=array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Satyrday"); 
       
        ?>
     
        <div class="blank-days">&nbsp<br>&nbsp</div>
        
        <?php
        for($x=0;$x<7;$x++){
               echo'<div class="days">';
               echo $arr_day[$x].'<br>'.$arr_date_day[$x];
               echo '</div>';    
        }
        ?>
           
        <?php
	
    include "../includes/connect.php"; // connect to db
    for($i=0;$i<=23;$i++)
    {
        echo '<div class="date">';
        echo '<div class="time-each-day">';
        echo $i.':00';
        echo '</div>';
        
       for($j=0;$j<7;$j++){
           $date = $year.'/'.$arr_date_month[$j].'/'.$arr_date_day[$j];
           echo'<div des="'.$date.'" time="'.$i.":00".'" class="each-day">';  
           echo "<br>";
            
           
        $query_user = mysqli_query($link,"SELECT * FROM event WHERE date = '$date' AND EXTRACT(HOUR FROM time) = '$i' AND users = '$session_username' ORDER BY time"); // query and select all data in table.
        while($num_rows_user = mysqli_fetch_array($query_user)){ // fetch array for read data in table.
            echo '<div num_id="'.$num_rows_user[0].'" class="title-user" onclick="show_detail('.$num_rows_user[0].')">';
            echo $num_rows_user[3];
            echo '</div>';
            }
             
           echo '</div>';
       } 
        echo '</div>';    
    }
        echo '<div class="form">'; // show detail
        echo '</div>';
        
?>    
   <div class="appointment"></div>
    </div>  
<script type="text/javascript">
  $( function() {
    $( ".form" ).dialog({
      autoOpen: false,
      show: {
        effect: "blind",
        duration: 1000,
        position : 'top'
      },
      hide: {
        effect: "fade",
        duration: 1000
      }
    });
  } );
    
function show_detail(i){
	$.ajax({
		url: "show-detail.php",
		data: "action=show&name="+i,
		type: 'post',
		dataType: 'html',
		success: function(data) {
			$('.form').html(data);
            $( ".form" ).dialog( "open" );
		}
	
	});
}
    function show_create(i,j){
	
	$.ajax({
		url: "appointment.php",
		data: "action=show&date="+i+"&time="+j,
		type: 'post',
		dataType: 'html',
		success: function(data) {
			$('.appointment').html(data);
            $( ".appointment" ).dialog( "open" );
		}
	
	});
}
         $(function() {
    $( ".appointment" ).dialog({
      autoOpen: false,
      resizable: false,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "fade",
        duration: 1000
      }
    });  
  } );
    
       $(function(){
    $(".title-user").draggable({
        helper: 'clone', // effect
    });
    $(".each-day").droppable({
        drop: function(e, ui) {
            var id = $(ui.draggable).attr('num_id'); 
            var html = $(ui.draggable).html();
            var date = $(this).attr('des'); 
            var time = $(this).attr('time'); 
            //alert(id);
            
            $.ajax({
               url: "../module/drag-edit-week.php",
               type: "get",
               data:{
                   'id':id,
                   'date':date,
                   'time':time
               },
                'success':function(){
                     location.reload();    
                }
            });
        }
    })
});
    
          $(function(){
    $(".title-relation").draggable({
        helper: 'clone', // effect
    });
    $(".each-day").droppable({
        drop: function(e, ui) {
            var id = $(ui.draggable).attr('num_id'); 
            var html = $(ui.draggable).html();
            var date = $(this).attr('des'); 
            var time = $(this).attr('time'); 
            //alert(id);
            
            $.ajax({
               url: "../module/drag-edit-week.php",
               type: "get",
               data:{
                   'id':id,
                   'date':date,
                   'time':time
               },
                'success':function(){
                     location.reload();    
                }
            });
        }
    })
});
    
            $(function(){
    $(".title-invite").draggable({
        helper: 'clone', // effect
    });
    $(".each-day").droppable({
        drop: function(e, ui) {
            var id = $(ui.draggable).attr('num_id'); 
            var html = $(ui.draggable).html();
            var date = $(this).attr('des'); 
            var time = $(this).attr('time'); 
            //alert(id);
            
            $.ajax({
               url: "../module/drag-edit-week.php",
               type: "get",
               data:{
                   'id':id,
                   'date':date,
                   'time':time
               },
                'success':function(){
                     location.reload();    
                }
            });
        }
    })
});
    
</script>
</body>
</html>