<?php
/**
 * Copyright (C) 2013 peredur.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once '../includes/db_connect.php';
include_once '../includes/functions.php';


if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
    <!DOCTYPE html>
    <html>
        <?php 
  
        
        ?>

    <head>
        <title>Log in</title>
        <link rel="stylesheet" href="../css/main.css" />
        <script type="text/JavaScript" src="../js/sha512.js"></script>
        <script type="text/JavaScript" src="../js/forms.js"></script>
        
    </head>
    <header>
        <?php include_once('../header.php') ?>
    </header>

<body>
        <?php
        if (isset($_GET['error'])) {
            echo '<p class="error">Error Logging In!</p>';
        }
        ?>
            <div class="form-login">
                <form action="../includes/process_login.php" method="post" name="login_form">
                    <div class="email_login">
                        <font class="text_login">E-mail :</font>
                        <input type="text" name="email" placeholder="example@email.com"> </div>
                    <div class="field"><font class="text_login">Password :</font>
                        <input type="password" name="password" id="password" placeholder="Password"></div>
                    <button class="login-button" type="button" onclick="formhash(this.form, this.form.password);"> Login </button>
                    <br>
                    <font class="text_login">Don't have an accout? <a  class="a_register" style="text-decoration: underline; " href="register.php">Register</a></font>
                    <br> </form>
            </div>
    </body>

    </html>