<?php
/**
 * Copyright (C) 2013 peredur.net
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once '../includes/db_connect.php';
include_once '../includes/functions.php';

?>
<!DOCTYPE html>
<html>
    <head>
            <link rel="stylesheet" href="../css/main.css" type="text/css">
            <script src="jscript.js"></script>
            <title>Appoointment</title>
        <?php include_once('../header.php') ?>
    </head>
    <body>
        <?php if (login_check($mysqli) == true) : ?>
        <?php if(isset($_POST['date']))
        {
            $date = $_POST['date'];
        }
        else $date = date('Y/m/d');
                
       if(isset($_POST['time'])){
            $time = $_POST['time'];
        }
        else
        {
            $time = "00:00";
        }
        ?>
        <form class="form_app" action="../module/insert.php" method="post" onsubmit="return validation()">
            <font class="text_header">Add Appointment</font>
            <div class="app_date">
               <font class="text_login"> Date :</font>
                
                <input  type="date"  name="date" value="<?php echo $day = date('Y-m-d',strtotime($date));?>" required> 
            </div>
                
            <div class="field"> 
                <font class="text_login">Time :</font>
                <input  type="time"  name="time" value="<?php echo $clock = date('H:i',strtotime($time));?>" required> </div>
            <div class="field"> 
                <font class="text_login">Title :</font>
                <input   class="app_text" name="title" placeholder="Title" oninput="check()" required> </div>

            <div class="field"> 
                <textarea  class="app_textarea" name="detail" placeholder="Write something here..."></textarea>
            </div>
            <button type="submit">Create</button>
        </form>
    
        <?php else : ?>
            <p>
                <center><span class="error">Please <a href="../pages/index.php">LOGIN</a> before adding an appointment.</span></center>
            </p>
        <?php endif; ?>
    </body>
</html>
