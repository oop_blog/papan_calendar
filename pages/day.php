<?php
    include_once '../includes/functions.php';
    sec_session_start();
    if(isset($_SESSION['username']))
    {
        $session_username = htmlentities($_SESSION['username']);
    }
    else $session_username = "Guest";
    date_default_timezone_set("Asia/Bangkok"); //set time zone
    $now = 'now'; // declare now for use in find year-month-day
    if(isset($_GET['now']) && !empty($_GET['now'])){
    $now = $_GET['now'];
    }
    $day = date('d',strtotime($now)); 
    $month = date('m',strtotime($now)); // get month for use in fucntion next and prev.
    $year = date('Y', strtotime($now)); // get year for use in fucntion next and prev.
    $firstday = date('w', strtotime('01-'.$month.'-'.$year)); // get day of week
    $days = date('t',strtotime($now)); // get number of days in month
    $today = date('d'); // get today
    $todaymonth = date('m'); // get month
    $todayyear = date('Y'); // get year
    
    $next_month = strtotime("+1 day $year-$month-$day"); // calculate timestamp next month
    $last_month = strtotime("-1 day $year-$month-$day"); // calculate timestamp last month 
    
    $get_next_year = date('Y', $next_month); // get year from $last_month
    $get_last_year = date('Y', $last_month); // get year from $next_month

    $get_next_month = date('m', $next_month); // get month from $last_month
    $get_last_month = date('m', $last_month); // get month from $next_month

    $get_next_day = date('d', $next_month); // get month from $last_month
    $get_last_day = date('d', $last_month); // get month from $next_month
    
    $to_lastmonth = "<a href=\"" . '?now=' . $get_last_year .'-'. $get_last_month . '-'. $get_last_day ."\"> ❮ </a>"; // link to lastmonth
    $to_nextmonth = "<a href=\"" . '?now=' . $get_next_year .'-'. $get_next_month . '-'. $get_next_day . "\"> ❯ </a>"; // link to nextmonth

    $check_month = $month; // check month to string
    switch ($check_month) {
        case "01":
        $check_month = "January";
        break;
        case "02":
         $check_month = "February";
        break;
        case "03":
         $check_month = "March";
        break;
        case "04":
         $check_month = "April";
        break;
        case "05":
         $check_month = "May";
        break;
        case "06":
         $check_month = "June";
        break;
        case "07":
         $check_month = "July";
        break;
        case "08":
         $check_month = "August";
        break;
        case "09":
         $check_month = "September";
        break;
        case "10":
         $check_month = "October";
        break;
        case "11":
         $check_month = "November";
        break;
        case "12":
         $check_month = "December";
        break;
}  
?>


<html>
<head>
    <title>Calendar</title>
     <link rel="stylesheet" href="../css/day.css" type="text/css">
    <link rel="stylesheet" href="../css/main.css" type="text/css">
    <link rel="stylesheet" href="../jquery-ui-1.12.1/jquery-ui.css">
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../jquery-ui-1.12.1/jquery-ui.js"></script>
    <style>
        .create{
            width: 10px;
            height: 10px;
            background:#dae4f3;
            float: left;
            margin-top:5px;
        }
    
    </style>
</head>

<body>
    <header>
        <div class="nav">
            <ul>
                <li style="float:left"><a href="../pages/appointment.php">Appointment</a></li>
                <li style="float:left"><a href="day.php?now=<?=$get_last_year;?>-<?=$get_last_month;?>-<?=$get_last_day;?>">Yesterday</a></li>
                <li class="dropdown" style="float:letf">
                    <a href="#" class="dropbtn">≡ Day</a>
                    <div class="dropdown-content">
                    <a href="month.php">Month</a>
                    <a href="week.php">Week</a>
                    </div>
                    
                </li>
                <li style="float:left"><a href="day.php?now=<?=$get_next_year;?>-<?=$get_next_month;?>-<?=$get_next_day;?>">Tomorrow</a></li>
                <<?php if($session_username == "Guest") { ?>   
            <li style="float:right"><a href="../pages/index.php">Log In</a></li>
            <li style="float:right"><a href="../pages/register.php">Sign Up</a></li>
            <?php } else { ?>
             <li style="float:right"><a href="../includes/logout.php">Logout</a></li>
            <li style="float:right"><a href="#"><?php echo $session_username; ?></a></li>
            <?php } ?>
            </ul>
        </div>
    </header>
    <div class="head-date">
        <font class="text_login">
    <?php echo  $day."/".$check_month."/".$year?>
        </font>
    </div>
    <div class="crop_calendar">
    <div class="calendar">

        <?php

    include "../includes/connect.php"; // connect to db
    for($i=0;$i<=23;$i++)
    {
        $date = $year.'/'.$month.'/'.$day;
        echo'<div des="'.$date.'" time="'.$i.":00".'" class="date';
        echo '">';
        echo '<a  onclick="show_create(\''.$date.'\',\''.$i.":00".'\')">';
        echo "$i:00";
        echo '</a>';
        echo ' &nbsp&nbsp::                                                                           ';
        echo "<br>";
       $query_user = mysqli_query($link,"SELECT * FROM event WHERE date = '$date' AND EXTRACT(HOUR FROM time) = '$i' AND users = '$session_username' ORDER BY time"); // query and select all data in table.
        while($num_rows_user = mysqli_fetch_array($query_user)){ // fetch array for read data in table.
            echo '<div num_id="'.$num_rows_user[0].'" class="title-user" onclick="show_detail('.$num_rows_user[0].')">';
            echo $num_rows_user[3];
            echo '</div>';
            echo '&nbsp';
            echo '&nbsp';
            }
  
        echo '</div>';        
    }
        echo '<div class="form"></div>';
        echo '<div class="appointment"></div>';
?>    
    </div>
    
<script type="text/javascript">
  $( function() {
    $( ".form" ).dialog({
      autoOpen: false,
      show: {
        effect: "fold",
        duration: 500
      },
      hide: {
        effect: "fade",
        duration: 500
      }
    });  
  } );
    
        
      $(function() {
    $( ".appointment" ).dialog({
      autoOpen: false,
      resizable: false,
      show: {
        effect: "fold",
        duration: 500
      },
      hide: {
        effect: "fade",
        duration: 500
      }
    });  
  } );
    
function show_detail(i){
	$.ajax({
		url: "show-detail.php",
		data: "action=show&name="+i,
		type: 'post',
		dataType: 'html',
		success: function(data) {
			$('.form').html(data);
            $( ".form" ).dialog( "open" );
		}
	
	});
}


        
function show_create(i,j){
	
	$.ajax({
		url: "appointment.php",
		data: "action=show&date="+i+"&time="+j,
		type: 'post',
		dataType: 'html',
		success: function(data) {
			$('.appointment').html(data);
            $( ".appointment" ).dialog( "open" );
		}
	
	});
}
    
    $(function(){
    $(".title-user").draggable({
        helper: 'clone', // effect
    });
    $(".date").droppable({
        drop: function(e, ui) {
            var id = $(ui.draggable).attr('num_id'); 
            var html = $(ui.draggable).html();
            var date = $(this).attr('des'); 
            var time = $(this).attr('time'); 
            //alert(id);
            
            $.ajax({
               url: "../module/drag-edit-day.php",
               type: "get",
               data:{
                   'id':id,
                   'date':date,
                   'time':time
               },
                'success':function(){
                     location.reload();    
                }
            });
        }
    })
});
</script>
    </div>
</body>
</html>


