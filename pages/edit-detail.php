<!DOCTYPE html>
<html>

<head>
    <?php include_once '../includes/functions.php';
    sec_session_start();
    ?>
    <link rel="stylesheet" href="../css/main.css" type="text/css">
    <script src="jscript.js"></script>
    <title>Edit Appointment</title>
    
</head>

<body>
    <header>
        <div class="nav">
            <ul>
                <li><a href="month.php">Calendar</a></li>
                <li style="float:right";><a href="../includes/logout.php">Logout</a></li>
                <li style="float:right";><a href="#"><?php echo htmlentities($_SESSION['username']);?></a></li>
            </ul>
        </div>
    </header>
    
    <div class="form_edit" style="
    margin-top: 50px;">
        <?php
        $num_id = $_GET['id'];
        $date = $_GET['date'];
        $time = $_GET['time'];
        $title = $_GET['title'];
        $detail = $_GET['detail'];
        ?>
        <form  action="../module/edit.php" method="post" onsubmit="return validation()">
            <input type="hidden" name="id" value="<?php echo $num_id;?>"/>
            <font class="text_header">Edit Appoitment</font>
            <div class="field"> 
                <font class="text_login">Date : </font>
                <input class="data" type="date"  name="date" value="<?php echo $date;?>"/></div>
             <div class="field"> 
                 <font class="text_login">Time :</font>
                <input class="data" type="time"  name="time" value="<?php echo $time;?>"> </div>
            <div class="field"> 
                <font class="text_login">Title :</font>
                <input class="app_text"  name="title" value="<?php echo $title;?>"/></div>
            <div class="field"> 
            <textarea  class="app_textarea" name="detail"><?php echo $detail;?></textarea>
        
            </div>
        
            <button type="submit">Edit</button>
        </form>
    </div>

</body>
</html>