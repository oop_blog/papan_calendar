<?php
/**
 * Copyright (C) 2013 peredur.net
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once '../includes/register.inc.php';
include_once '../includes/functions.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration</title>
        <script type="text/JavaScript" src="../js/sha512.js"></script> 
        <script type="text/JavaScript" src="../js/forms.js"></script>
        <link rel="stylesheet" href="../css/main.css" />
    </head>
    <header>
        <?php include_once('../header.php') ?>
    </header>
    <body>
        <!-- Registration form to be output if the POST variables are not
        set or if the registration script caused an error. -->
        
        <div class="container">
           
            <!--
            <div class="rule">
            <ul>
            <p></p>Usernames may contain only digits, upper and lower case letters and underscores</p>
            <p>Emails must have a valid email format</p>
            <p>Passwords must be at least 6 characters long</p>
            <p>Passwords must contain
                <ul>
                    <p>At least one upper case letter (A..Z)</p>
                    <p>At least one lower case letter (a..z)</p>
                    <p>At least one number (0..9)</p>
                </ul>
            
            <li>Your password and confirmation must match exactly</li>
            </div>
            -->
        
        <div class="form_regis">
       
        <form method="post" name="registration_form" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>">
            <div class="user_register">
            <font class="text_login">Username :</font>
            <input  placeholder="Username" type='text' name='username' id='username' />
            </div>
            <div class="email_register">
            <font class="text_login">E-mail :</font>
            <input  placeholder="example@email.com" type="text" name="email" id="email" />  
            </div>
            <div class="pass_register">
            <font class="text_login">Password :</font>
            <input  placeholder="Password" type="password" name="password" id="password"/> 
            </div>
            <div class="field">
            <font class="text_login">Re-Password :</font>
            <input  placeholder="Re - password" type="password" name="confirmpwd" id="confirmpwd" />
            </div>
            <?php
        if (!empty($error_msg)) {
            echo $error_msg;
        }
        ?>
            <button class="regis_button" onclick="return regformhash(this.form,this.form.username,this.form.email, this.form.password,this.form.confirmpwd);" >Register</button>
            
            <br>
        </form>
        </div>
        </div>
    </body>
</html>
