<?php
    include_once '../includes/functions.php';
    sec_session_start();  
    if(isset($_SESSION['username'])){ // เช็คว่ามีการ login ไหม ถ้ามีแสดงชื่อ user 
     $session_username = htmlentities($_SESSION['username']);
    }
    else{ // แสดง guest เมื่อไม่มีการ login
         $session_username = "Guest";
    }
    date_default_timezone_set("Asia/Bangkok"); //set time zone
    $now = 'now'; // declare now for use in find year-month-day
    if(isset($_GET['now']) && !empty($_GET['now'])){
    $now = $_GET['now'];
    }
    $day = date('d',strtotime($now)); 
    $month = date('m',strtotime($now)); // get month for use in fucntion next and prev.
    $year = date('Y', strtotime($now)); // get year for use in fucntion next and prev.
    $firstday = date('w', strtotime('01-'.$month.'-'.$year)); // get day of week
    $days = date('t',strtotime($now)); // get number of days in month
    $today = date('d'); // get today
    $todaymonth = date('m'); // get month
    $todayyear = date('Y'); // get year
        
    $last_month = strtotime("-1 month $year-$month"); // calculate timestamp last month 
    $next_month = strtotime("+1 month $year-$month"); // calculate timestamp next month
  
    $get_next_year = date('Y', $next_month); // get year from $last_month
    $get_last_year = date('Y', $last_month); // get year from $next_month

    $get_next_month = date('m', $next_month); // get month from $last_month
    $get_last_month = date('m', $last_month); // get month from $next_month
    
    $to_lastmonth = "<a href=\"" . '?now=' . $get_last_year .'-'.$get_last_month . "\"> ❮ </a>"; // link to lastmonth
    $to_nextmonth = "<a href=\"" . '?now=' . $get_next_year .'-'.$get_next_month . "\"> ❯ </a>"; // link to nextmonth

    $check_month = $month; // check month to string
    switch ($check_month) {
        case "01":
        $check_month = "January";
        break;
        case "02":
         $check_month = "February";
        break;
        case "03":
         $check_month = "March";
        break;
        case "04":
         $check_month = "April";
        break;
        case "05":
         $check_month = "May";
        break;
        case "06":
         $check_month = "June";
        break;
        case "07":
         $check_month = "July";
        break;
        case "08":
         $check_month = "August";
        break;
        case "09":
         $check_month = "September";
        break;
        case "10":
         $check_month = "October";
        break;
        case "11":
         $check_month = "November";
        break;
        case "12":
         $check_month = "December";
        break;
}  
?>


<html>
<head>
    <title>Calendar</title>
     <link rel="stylesheet" href="../css/month.css" type="text/css">
    <link rel="stylesheet" href="../css/main.css" type="text/css">
    <link rel="stylesheet" href="../jquery-ui-1.12.1/jquery-ui.css">
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../jquery-ui-1.12.1/jquery-ui.js"></script>
</head>

<body>
    <header>
        <div class="nav">
            <ul>
                <li style="float:left"><a href="../pages/appointment.php">Appointment</a></li>
                <li style="float:left"><a href="month.php?now=<?=$get_last_year;?>-<?=$get_last_month;?>">Previous Month</a></li>
                <li class="dropdown" style="float:letf">
                    <a href="#" class="dropbtn">≡ Month</a>
                    <div class="dropdown-content">
                    <a href="day.php">Day</a>
                    <a href="week.php">Week</a>
                    </div>
                    
            </li>
                <li style="float:left"><a href="month.php?now=<?=$get_next_year;?>-<?=$get_next_month;?>">Next Month</a></li>
            <?php if($session_username == "Guest") { ?>   
            <li style="float:right"><a href="../pages/index.php">Log In</a></li>
            <li style="float:right"><a href="../pages/register.php">Sign Up</a></li>
            <?php } else { ?>
             <li style="float:right"><a href="../includes/logout.php">Logout</a></li>
            <li style="float:right"><a href="#"><?php echo $session_username; ?></a></li>
            <?php } ?>
            </ul>
        </div>
    </header>
    <div class="head-date">
        <font class="text_login">
        <?php echo  $check_month."/".$year ?></font>
        </div>
    <div class="crop_calendar">
    <div class="calendar">
        
        
        <div class="days">Sunday</div>
        <div class="days">Monday</div>
        <div class="days">Tuesday</div>
        <div class="days">Wednesday</div>
        <div class="days">Thursday</div>
        <div class="days">Friday</div>
        <div class="days">Saturday</div>
       
        <?php
	  for($i=1;$i<=$firstday;$i++) // days blank
        {
            echo '<div class="date blankday"></div>';
        } 

    include "../includes/connect.php"; // connect to db
    for($i=1;$i<=$days;$i++)
    {
        $date = $year.'-'.$month.'-'.$i;
        echo'<div des="'.$date.'" class="date';
        if($today == $i && $todaymonth == $month && $todayyear == $year) // today.
        {
            echo '-active';
        }
        echo '" > ';
        echo '<a  onclick="show_create(\''.$date.'\')">';
        echo "$i";
         echo '</a>';
        echo '<br>';
       
        
           $query_user = mysqli_query($link,"SELECT * FROM event WHERE date = '$date' AND users = '$session_username' "); // query user
          while($num_rows_user = mysqli_fetch_array($query_user)){ // fetch array for read data in table.
            
            echo '<div class="cover">';
            echo '<div num_id="'.$num_rows_user[0].'" class="title-user" onclick="show_detail('.$num_rows_user[0].')">';
            echo $num_rows_user[3];
            echo '</div>';
            echo '</div>';
            }
       
    
        echo '</div>';         
    }
        
        
        
        

    $daysleft = 7-(($days + $firstday)%7); // daysleft in each month
    if($daysleft<7)
    {
        for($i=1;$i<=$daysleft;$i++)
        {
            echo '<div class="date blankday"></div>';
        }
    }

?>    
    </div>
    
    <div class="form"></div>'
    <div class="appointment"></div>
       
     
<script type="text/javascript">
  $( function() {
    $( ".form" ).dialog({
      autoOpen: false,
      resizable: false,
      show: {
        effect: "fold",
        duration: 500
      },
      hide: {
        effect: "fade",
        duration: 500
      }
    });  
  } );
    
      $(function() {
    $( ".appointment" ).dialog({
      autoOpen: false,
      resizable: false,
      show: {
        effect: "fold",
        duration: 500
      },
      hide: {
        effect: "fade",
        duration: 500
      }
    });  
  } );
    
      
function show_detail(i){
	
	$.ajax({
		url: "show-detail.php",
		data: "action=show&name="+i,
		type: 'post',
		dataType: 'html',
		success: function(data) {
			$('.form').html(data);
            $( ".form" ).dialog( "open" );
		}
	
	});
}
    
function show_create(i){
	
	$.ajax({
		url: "appointment.php",
		data: "action=show&date="+i,
		type: 'post',
		dataType: 'html',
		success: function(data) {
			$('.appointment').html(data);
            $( ".appointment" ).dialog( "open" );
		}
	
	});
}
    
    $(function(){
    $(".title-user").draggable({
        helper: 'clone', 
    });
    $(".date,.date-active").droppable({
        drop: function(e, ui) {
            var id = $(ui.draggable).attr('num_id'); 
            var html = $(ui.draggable).html();
            var date = $(this).attr('des'); 
            //alert(id);
            
            $.ajax({
               url: "../module/drag-edit.php",
               type: "get",
               data:{
                   'id':id,
                   'date':date
               },
                'success':function(){
                     location.reload();    
                }
            });
        }
    })
});
    
         $(function(){
    $(".title-relation").draggable({
        helper: 'clone',
    });
    $(".date,.date-active").droppable({
        drop: function(e, ui) {
            var id = $(ui.draggable).attr('num_id');
            var html = $(ui.draggable).html();
            var date = $(this).attr('des');
            //alert(id);
            
            $.ajax({
               url: "../module/drag-edit.php",
               type: "get",
               data:{
                   'id':id,
                   'date':date
               },
                'success':function(){
                     location.reload();    
                }
            });
        }
    })
});
    
    
        $(function(){
    $(".title-invite").draggable({
        helper: 'clone',
    });
    $(".date,.date-active").droppable({
        drop: function(e, ui) {
            var id = $(ui.draggable).attr('num_id');
            var html = $(ui.draggable).html();
            var date = $(this).attr('des');
            //alert(id);
            
            $.ajax({
               url: "../module/drag-edit.php",
               type: "get",
               data:{
                   'id':id,
                   'date':date
               },
                'success':function(){
                     location.reload();    
                }
            });
        }
    })
});
</script>
</div>
</body>
</html>