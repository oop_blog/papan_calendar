<?php 
include_once 'includes/functions.php';
    sec_session_start();  
    if(isset($_SESSION['username'])){ // เช็คว่ามีการ login ไหม ถ้ามีแสดงชื่อ user 
     $session_username = htmlentities($_SESSION['username']);
    }
    else{ // แสดง guest เมื่อไม่มีการ login
         $session_username = "Guest";
    }
    $now = 'now';
    $day = date('d',strtotime($now)); 
    $month = date('m',strtotime($now)); // get month for use in fucntion next and prev.
    $year = date('Y', strtotime($now)); // get year for use in fucntion next and prev.
    $firstday = date('w', strtotime('01-'.$month.'-'.$year)); // get day of week
    $today = date('d'); // get today
    $todaymonth = date('m'); // get month
    $todayyear = date('Y'); // get year
    
    $next_month = strtotime("+1 day $year-$month-$day"); // calculate timestamp next month
    $last_month = strtotime("-1 day $year-$month-$day"); // calculate timestamp last month 
    
    $get_next_year = date('Y', $next_month); // get year from $last_month
    $get_last_year = date('Y', $last_month); // get year from $next_month

    $get_next_month = date('m', $next_month); // get month from $last_month
    $get_last_month = date('m', $last_month); // get month from $next_month

    $get_next_day = date('d', $next_month); // get month from $last_month
    $get_last_day = date('d', $last_month); // get month from $next_month
    
    $to_lastmonth = "<a href=\"" . '?now=' . $get_last_year .'-'. $get_last_month . '-'. $get_last_day ."\"> ❮ </a>"; // link to lastmonth
    $to_nextmonth = "<a href=\"" . '?now=' . $get_next_year .'-'. $get_next_month . '-'. $get_next_day . "\"> ❯ </a>"; // link to nextmonth

?>
<div class="nav">
            <ul>
                <li style="float:left"><a href="../pages/appointment.php">Appointment</a></li>
                <li class="dropdown" style="float:letf">
                    <a href="#" class="dropbtn">≡ Calendar</a>
                    <div class="dropdown-content">
                    <a href="month.php">Month</a>
                    <a href="day.php">Day</a>
                    <a href="week.php">Week</a>
                    </div>
                    
            </li>
                <?php if($session_username == "Guest") { ?>
            
            <li style="float:right"><a href="../pages/index.php">Log In</a></li>
            <li style="float:right"><a href="../pages/register.php">Sign Up</a></li>
            <?php } else { ?>
             <li style="float:right"><a href="../includes/logout.php">Logout</a></li>
            <li style="float:right"><a href="#"><?php echo $session_username; ?></a></li>
            <?php } ?>
            </ul>
        </div>